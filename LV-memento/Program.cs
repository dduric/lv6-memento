﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV_memento
{
    class Program
    {
        static void Main(string[] args)
        {
            CareTaker caretaker = new CareTaker();

            ToDoItem item = new ToDoItem("Show Example", "Pokazi memento", DateTime.Now.AddDays(1));

            Console.WriteLine(item);

            // Memento checkpoint = item.StoreState();

            caretaker.PersistState("initial", checkpoint);

            item.ChangeTimeDue(DateTime.Now.AddDays(4));

            Console.WriteLine(item);

            //  item.RestoreState(caretaker.RetriveState("initial"));

            Console.WriteLine(item);


            Console.WriteLine();
            Console.WriteLine();

            BankAccount pbz = new BankAccount("Dario", "Eszek", 100);
            Console.WriteLine(pbz);



        }
    }
}
