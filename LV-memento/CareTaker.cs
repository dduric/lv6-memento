﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_memento
{
    class CareTaker
    {
        Dictionary<String, Memento> states;

        public CareTaker()
        {
            states = new Dictionary<string, Memento>();
        }

        // public Memento PreviousState { get; set; }

        public void PersistState(String key, Memento state)
        {
            states.Add(key, state);
        }

        public Memento RetriveState(String key)
        {
            return states[key];
        }


    }
}
